﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GASimpleExample
{
    [DebuggerDisplay("{Word,nq} - {GetFitness(),nq}")]
    public class WordIndividual
    {
        public string Word { get; set; }

        public const string Pattern = "Hello world!";
        const string Sequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz! ";

        public int PatternLength => Pattern.Length;

        public int GetFitness()
        {
            var fitness = Word.Count(c => Pattern.Contains(c));

            for (int i = 0; i < PatternLength; i++)
            {
                if (Word[i] == Pattern[i])
                    fitness += 1;
            }

            return fitness;
        }

        public void SeedWord()
        {
            var chars = new char[PatternLength];
            var random = new Random();
            for (int i = 0; i < Pattern.Length; i++)
            {
                chars[i] = Pattern[random.Next(0, PatternLength)];
            }

            Word = new string(chars);
        }

        private int GetFitnessOfChar(int index)
        {
            var fitness = 0;
            if (Sequence.Contains(Word[index]))
                fitness++;
            if (Pattern.Contains(Word[index]))
                fitness++;
            if (Pattern[index] == Word[index])
                fitness++;

            return fitness;
        }

        public WordIndividual CrossOver(WordIndividual other)
        {
            var random = new Random();
            var swapRange = random.Next(0, PatternLength);

            var swappedChars = new char[Pattern.Length];

            for (int i = 0; i < PatternLength; i++)
            {
                if (other.GetFitnessOfChar(i) > GetFitnessOfChar(i))
                    swappedChars[i] = other.Word[i];
            }

            for (int i = 0; i < PatternLength; i++)
            {
                if (other.GetFitnessOfChar(i) < GetFitnessOfChar(i))
                    swappedChars[i] = Word[i];
            }

            //heh, creating new strings every time is bad, i don't give a fuck
            return new WordIndividual { Word = new string(swappedChars) };
        }

        public void Mutate()
        {
            var random = new Random();
            var randomizationIndex = random.Next(0, PatternLength);
            var existingChars = Word.ToArray();
            existingChars[randomizationIndex] = Pattern[random.Next(0, PatternLength)];

            Word = new string(existingChars);
        }
    }

    class Program
    {
        const int SizeOfPopulation = 1024;

        static void Main(string[] args)
        {
            var population = new List<WordIndividual>(SizeOfPopulation);

            Parallel.For(0, SizeOfPopulation, i =>
            {
                var individual = new WordIndividual();
                individual.SeedWord();
                population.Add(individual);
            });

            var currentPopulation = new List<WordIndividual>(population);
            var epochFitness = 0;
            var populationCount = 1;
            var findedIndividual = (WordIndividual)null;

            while (currentPopulation.Count != 0 && findedIndividual == null)
            {
                var nextPopulation = new List<WordIndividual>();
                var bestPopulation = new List<WordIndividual>();
                var random = new Random();
                var max = currentPopulation.Max(item => item.GetFitness());

                foreach (var item in currentPopulation)
                {
                    var selecting = currentPopulation[random.Next(0, currentPopulation.Count)];
                    var best = item.GetFitness() > selecting.GetFitness() ? item : selecting;
                    if (best.Word == WordIndividual.Pattern)
                    {
                        findedIndividual = best;
                        break;
                    }

                    bestPopulation.Add(best);
                }

                if (findedIndividual != null)
                    break;

                foreach (var individual in bestPopulation)
                {
                    var crossing = currentPopulation.ElementAt(random.Next(0, currentPopulation.Count));

                    var crossed = individual.CrossOver(crossing);
                    if(crossed.Word == WordIndividual.Pattern)
                    {
                        findedIndividual = crossed;
                        break;
                    }

                    nextPopulation.Add(crossed);
                }

                if (findedIndividual != null)
                    break;

                if (populationCount % 5 == 0)
                {
                    var maxInNexPopulation = nextPopulation.Max(item => item.GetFitness());
                    if (epochFitness == maxInNexPopulation)
                    {
                        for (int i = 0; i < nextPopulation.Count / 10; i++)
                        {
                            var mutatingIndividual = nextPopulation[random.Next(0, nextPopulation.Count)];
                            mutatingIndividual.Mutate();
                        }
                    }
                    else
                    {
                        epochFitness = maxInNexPopulation;
                    }
                }

                var maxInNext = nextPopulation.Max(item => item.GetFitness());

                var maximumIndividual = nextPopulation.First(item => item.GetFitness() == maxInNext);

                Console.WriteLine($"Max fitness in current population: {maxInNext}, current word: {maximumIndividual.Word}");

                currentPopulation = nextPopulation;
                populationCount++;
            }

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Word of finded individual {0}", findedIndividual.Word);
        }
    }
}
